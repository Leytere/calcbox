#ifndef DEF_CALCBOX
#define DEF_CALCBOX

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <stdio.h>

class CalcBox {
    public:
        CalcBox(std::string path);
        ~CalcBox();

        std::vector <double> getBoxes() const;
        void setBox(int id, double newContent);
        std::string getPath() const;

        std::string displayId(int id) const;
        void displayAll() const;
        void pushAndDisplayResult(int destId, double stock);

        void addBoxes(int addId1, int addId2, int destId);
        void subBoxes(int subId1, int subId2, int destId);
        void mulBoxes(int mulId1, int mulId2, int destId);
        void divBoxes(int divId1, int divId2, int destId);
        void switchBoxes(int swiId1, int swiId2);

        void eraseBox(int id);
        void createBox(double value);
        void deleteAllBoxes(int size);

        void deleteLastLine() const;
        void changeLine(std::string newLine, int id) const;

    private:
        std::vector <double> boxes_;
        std::string path_;
};

#endif
