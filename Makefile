main: main.o CalcBox.o Menu.o
	g++ -o main main.o CalcBox.o Menu.o

main.o: main.C
	g++ -c -o main.o main.C

CalcBox.o: CalcBox.C
	g++ -c -o CalcBox.o CalcBox.C

Menu.o: Menu.C
	g++ -c -o Menu.o Menu.C
