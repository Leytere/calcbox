#ifndef DEF_MENU
#define DEF_MENU

#include <iostream>
#include <string>
#include <vector>
#include <limits>
#include "CalcBox.h"

class Menu {
    public:
        Menu(CalcBox *calcBox);
        ~Menu();

        void displayMainMenu();
        void displayByIdMenu(int size);
        void invalidMsg(int min, int max);
        void coutMenu() const;

        void boxesOperationMenu1(int size, std::string operationChoice);
        void boxesOperationMenu2(int size, int id1, std::string operationChoice);
        void boxesOperationMenu3(int size, int id1, int id2, std::string operationChoice);
        void boxesOperationMenu4(int size, int id1, int id2, int destId, std::string operationChoice);

        void deleteBoxMenu(int size);
        void deleteAllBoxesMenu(int size);
        void createBoxMenu(int size);

    private:
        CalcBox *calcBox_;
        int userOptionChoice_;
        double userStockChoice_;
};

#endif
